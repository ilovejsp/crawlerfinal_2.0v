package net.care2i.manage;

/**
 * <pre>
 * 관리 root 인터페이스
 * </pre>
 * @author ilovejsp
 * 
 */
public interface Manage {
	/**
	 * 
	 * @param name 관리하는 객체
	 * @return 제대로 관리중인지 check 하는 변수
	 */
	public boolean ManageResource(Object name);
}
