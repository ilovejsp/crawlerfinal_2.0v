package net.care2i.manage;

import java.util.HashMap;

import net.care2i.tempdto.SearchKeyword;
/**
 * <pre>
 * 검색해주는 인터페이스
 * </pre>
 * @author ilovejsp
 * 검색해주는 역활 
 */
public interface CollectorManage extends Manage{
	/**
	 * <pre>
	 * 리소스관리하는 메소드
	 * </pre>
	 * @param name 관리객체
	 * @return T/F 리턴
	 */
	public boolean ManageCollectorResource(Object name);
	/**
	 * <pre>
	 * 해당키워드로 검색해주는 메소드
	 * </pre>
	 * @param searchkey 키워드
	 * @return SearchKeyword객체
	 */
	public SearchKeyword SearchManage(SearchKeyword searchkey);
	/**
	 * 문자열로 검색해주는 메소드
	 * @param key 검색키워드
	 * @return 검색한 html페이지를 string으로 변환한 값
	 */
	public String SearchManage(String key);
	/**
	 * <pre>
	 * 해당페이지 스크래치하는 메소드
	 * </pre>
	 * @param url 스크래치 할 주소
	 * @return 스크래치한 소스
	 */
	public String ScratchManage(String url);
}
