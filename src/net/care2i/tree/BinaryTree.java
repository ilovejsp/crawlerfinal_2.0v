package net.care2i.tree;

import java.util.ArrayList;

public class BinaryTree {
	Binary root;
	private ArrayList<String> returnList = new ArrayList<String>();
	
	public Binary search(String keyword) {
		Binary returnValue=searchNode(root, keyword);
		return returnValue;
	}
	public Binary searchNode(Binary point,String keyword) {
		if (point == null) {
			System.out.println("찾는데이터가 없습니다.");
		}else if(!point.getKeyword().equals(keyword)) {
			searchNode(point.getRight(), keyword);
			searchNode(point.getLeft(), keyword);
		}else{
			System.out.println("찾은 키워드는 : " + point.getKeyword());
			System.out.println("찾은 링크는 : "+point.getLink());
			System.out.println("찾은 정확도는 : "+point.getAccuracy());
			return point;
		}
		return point;
	}
	public Binary selectAll(){
		if(root==null){
			return null;
		}else{
			return root;
		}
	}
	/**
	 * <pre>
	 * 이진트리 삽입메소드
	 * </pre>
	 * @param accuracy 정확도
	 */
	public void insert(Binary binary) {
		Binary newNode = new Binary();
		newNode.setAccuracy(binary.getAccuracy());
		newNode.setKeyword(binary.getKeyword());
		newNode.setLeft(binary.getLeft());
		newNode.setLink(binary.getLink());
		if (root == null) {
			root = newNode;
		} else {
			insertNode(root, newNode);
		}
	}
	/**
	 * <pre>
	 * 이진트리 삽입메소드
	 * </pre>
	 * @param point 루트이진트리
	 * @param newNode 새로운이진트리
	 */
	public void insertNode(Binary point, Binary newNode) {
		if (point.getAccuracy() <= newNode.getAccuracy()) {
			if (point.getRight() == null) {
				point.setRight(newNode);
			} else {
				insertNode(point.getRight(), newNode);
			}
		} else if (point.getAccuracy() > newNode.getAccuracy()) {
			if (point.getLeft() == null) {
				point.setLeft(newNode);
			} else {
				insertNode(point.getLeft(), newNode);
			}
		} else {
			System.out.println("삽입실패");
		}
	}
	/**
	 * <pre>
	 * 프린트하는 메소드
	 * </pre>
	 */
	public void print() {
		inorder(root);
		System.out.println();
	}
	
	/**
	 * <pre>
	 * 정렬하는 메소드
	 * </pre>
	 * @param root
	 */
	public void inorder(Binary root) {
		if (root != null) {
			inorder(root.getLeft());
			System.out.print(root.getAccuracy() + " ");
			inorder(root.getRight());
		}
	}
	public ArrayList<String> printLink() {
		inorderLink(root);
		return returnList;
	}
	
	public void inorderLink(Binary root) {
		if (root != null) {
			inorderLink(root.getLeft());
			returnList.add(root.getLink());
			//System.out.println(root.getLink());
			inorderLink(root.getRight());
		}
	}
	
}
