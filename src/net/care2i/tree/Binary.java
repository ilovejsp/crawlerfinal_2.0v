package net.care2i.tree;
/**
 * <pre>
 * 이진트리클래스
 * </pre>
 * @author ilovejsp
 *
 */
public class Binary {
	private Binary left;
	private Binary right;
	private String keyword;
	private String link;
	private Double accuracy;//기준
	/**
	 * @return 이진트리 왼쪽값
	 */
	public Binary getLeft() {
		return left;
	}
	/**
	 * @param left 이진트리왼쪽자식노드
	 */
	public void setLeft(Binary left) {
		this.left = left;
	}
	/**
	 * 
	 * @return 이진트리오른쪽자식노드
	 */
	public Binary getRight() {
		return right;
	}
	/**
	 * @param right 이진트리오른쪽자식노드
	 */
	public void setRight(Binary right) {
		this.right = right;
	}
	/**
	 * 
	 * @return keyword값
	 */
	public String getKeyword() {
		return keyword;
	}
	/**
	 * 
	 * @param keyword keyword값
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	/**
	 * 
	 * @return link값
	 */
	public String getLink() {
		return link;
	}
	/**
	 * 
	 * @param link link값
	 */
	public void setLink(String link) {
		this.link = link;
	}
	/**
	 * 
	 * @return 정확도
	 */
	public Double getAccuracy() {
		return accuracy;
	}
	/**
	 * 
	 * @param accuracy 정확도
	 */
	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}
}
