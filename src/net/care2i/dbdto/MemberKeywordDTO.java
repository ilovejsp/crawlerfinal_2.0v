package net.care2i.dbdto;
/**
 * 
 * @author ilovejsp
 * 멤버키워드 DTO
 */
public class MemberKeywordDTO {
	private int no;
	private int mkeycno;
	private int memberno;
	/**
	 * 
	 * @return 멤버키워드테이블 key
	 */
	public int getNo() {
		return no;
	}
	/**
	 * 
	 * @param no 멤버키워드테이블 key
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * 
	 * @return 멤버키워드카테고리 테이블 외부키
	 */
	public int getMkeycno() {
		return mkeycno;
	}
	/**
	 * 
	 * @param mkeycno 멤버키워드카테고리 테이블 외부키
	 */
	public void setMkeycno(int mkeycno) {
		this.mkeycno = mkeycno;
	}
	/**
	 * 
	 * @return 회원테이블 외부 key
	 */
	public int getMemberno() {
		return memberno;
	}
	/**
	 * 
	 * @param memberno 회원테이블 외부 key
	 */
	public void setMemberno(int memberno) {
		this.memberno = memberno;
	}
	
}
