package net.care2i.dbdto;
/**
 * <pre>
 * 링크저장객체
 * </pre>
 * @author ilovejsp
 * 링크 저장 데이터베이스 DTO
 */
public class LinkStoreDTO {
	private int no;
	private String link;
	private double accuracy;
	/**
	 * <pre>
	 * link key 값관련 getter메소드
	 * </pre>
	 * @return 링크key값
	 */
	public int getNo() {
		return no;
	}
	/**
	 * <pre>
	 * link key 값관련 setter메소드
	 * </pre>
	 * @param no linkkey값
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * <pre>
	 * 링크키워드 관련 getter메소드
	 * </pre>
	 * @return 링크키워드
	 */
	public String getLink() {
		return link;
	}
	/**
	 * <pre>
	 * link키워드관련 setter메소드
	 * </pre>
	 * @param link 링크키워드
	 */
	public void setLink(String link) {
		this.link = link;
	}
	/**
	 * <pre>
	 * 정확도 관련 getter메소드
	 * </pre>
	 * @return 정확도
	 */
	public double getAccuracy() {
		return accuracy;
	}
	/**
	 * <pre>
	 * 링크관련 setter메소드 
	 * </pre>
	 * @param accuracy 정확도
	 */
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}
	
}
