package net.care2i.dbdto;
/**
 * <pre>
 * 휴지통관련 dto클래스
 * </pre>
 * @author ilovejsp
 */
public class GarbageDTO {
	private int no;//휴지통 key값
	private int linksno;//link key 값
	/**
	 * <pre>
	 * 휴지통 관련 getter 메소드
	 * </pre>
	 * @return no 휴지통 key값
	 */
	public int getNo() {
		return no;
	}
	/**
	 * <pre>
	 * 휴지통 관련 setter메소드
	 * </pre>
	 * @param no 휴지통key값
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * <pre>
	 * link 관련 getter메소드
	 * </pre>
	 * @return linkkey값
	 */
	public int getLinksno() {
		return linksno;
	}
	/**
	 * <pre>
	 * link 관련 setter메소드
	 * </pre>
	 * @return link key 값
	 */	public void setLinksno(int linksno) {
		this.linksno = linksno;
	}
	
}
