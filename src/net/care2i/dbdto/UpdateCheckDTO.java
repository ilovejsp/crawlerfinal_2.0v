package net.care2i.dbdto;
/**
 * 
 * @author ilovejsp
 * 업데이트 체크 DB
 */
public class UpdateCheckDTO {
	private int no;
	private String date;
	private int linksno;
	/**
	 * 
	 * @return key값
	 */
	public int getNo() {
		return no;
	}
	/**
	 * 
	 * @param no key값
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * 
	 * @return 최신업데이트날짜
	 */ 
	public String getDate() {
		return date;
	}
	/**
	 * 
	 * @param date 최신업데이트날짜
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * 
	 * @return 링크key값
	 */
	public int getLinksno() {
		return linksno;
	}
	/**
	 * 
	 * @param linksno 링크key값
	 */
	public void setLinksno(int linksno) {
		this.linksno = linksno;
	}
	
}
