package net.care2i.dbdto;
/**
 * <pre>
 * 링크저장키워드 데이터베이스
 * </pre>
 * @author ilovejsp
 */
public class LinkStoreKeywordDTO {
	private int no;
	private String keyword;
	private int linksno;
	private int priority;
	/**
	 * <pre>
	 * 링크저장키워드 관련 getter메소드
	 * </pre>
	 * @return 링크저장키워드 key값
	 */
	public int getNo() {
		return no;
	}
	/**
	 * <pre>
	 * 링크저장키워드 관련 setter메소드
	 * </pre>
	 * @param no 링크저장키워드 key값
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * <pre>
	 * 키워드관련 getter메소드
	 * </pre>
	 * @return 키워드
	 */
	public String getKeyword() {
		return keyword;
	}
	/**
	 * <pre>
	 * 키워드관련 setter메소드
	 * </pre>
	 * @param keyword 키워드
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	/**
	 * <pre>
	 * 링크관련번호 getter메소드
	 * </pre>
	 * @return 링크번호key값
	 */
	public int getLinksno() {
		return linksno;
	}
	/**
	 * <pre>
	 * 링크관련번호 setter매소드
	 * </pre>
	 * @param linksno 링크번호
	 */
	public void setLinksno(int linksno) {
		this.linksno = linksno;
	}
	/**
	 * <pre>
	 * 우선순위 관련 getter메소드
	 * </pre>
	 * @return 우선순위값 
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * <pre>
	 * 우선순위 관련 setter메소드
	 * <pre>
	 * @param priority 우선순위값
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
}
