package net.care2i.webcrawler;

import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jaunt.*;

import net.care2i.accuracy.Accuracy;
import net.care2i.combine.Combine;
import net.care2i.dbdto.MemberKeywordCategoryDTO;
import net.care2i.manage.MatchManage;
import net.care2i.rearrage.Rearrange;
import net.care2i.tempdto.AccuracyDTO;
import net.care2i.tempdto.KeywordDTO;
/**
 * <pre>
 * 키워드 관련 클래스
 * </pre>
 * @author ilovejsp
 */

public class Match implements Accuracy, Combine, Rearrange,MatchManage {
	static ArrayList<String> returnlist = new ArrayList<String>();	
	static Crawler crawler = new Crawler();
	
	@Override
	public ArrayList<String> getcombination(ArrayList<String> list) {
		String id=list.get(0);
		System.out.println(id);
		ArrayList<String> finalReturnList = new ArrayList<String>();	
		list.remove(0);
		for(int i=1 ;i<=list.size() ; i++){
			combination(i, list);
		}	
		for(int i=0; i<returnlist.size(); i++){
			finalReturnList.add(id+" "+returnlist.get(i));
		}
		return finalReturnList;
	}
	
	@Override	
	public void combination(int size,ArrayList<String> arr) {
		int[] selected =new int[size];
		print(size, arr, 0, selected);
	}
	
	@Override	
	public void print(int size,ArrayList<String> arr,int from,final int[] selected) {
		String temp="";
		if (size == 0) {
			for (int i = 0; i < selected.length; ++i) {
				temp=temp+arr.get(selected[i])+" ";
			}
			returnlist.add(temp);
			temp="";
			return;
		}
		for (int i = from; i < arr.size(); ++i) {
			selected[selected.length - size] = i;
			print(size - 1, arr, i + 1, selected);
		}
	}
	
/*	
	@Override
	public ArrayList<String> getcombination(KeywordDTO keyworddto) {
		for(int i=1 ;i<=keyworddto.getList().size()+1 ; i++){
			combination(i, keyworddto.getList());
		}	
		return returnlist;
	}
	@Override	
	public void combination(int size,ArrayList<HashMap<String, Double>> arr) {
		int[] selected =new int[size];
		print(size, arr, 0, selected);
	}
	@Override	
	public void print(int size,ArrayList<HashMap<String, Double>> arr,int from,final int[] selected) {
		String temp="";
		if (size == 0) {
			for (int i = 0; i < selected.length; ++i) {
				System.out.print(arr.get(selected[i]).keySet() + " ");
				temp=temp+arr.get(selected[i]).keySet()+" ";
			}
			System.out.println();
			returnlist.add(temp);
			temp="";
			return;
		}
		for (int i = from; i < arr.size(); ++i) {
			selected[selected.length - size] = i;
			print(size - 1, arr, i + 1, selected);
		}
	}
*/	
	@Override
	public List<String> RearrangeResult(List<String> list, int priority) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean ManageResource(Object name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ManageMatchResource(Object name) {
		// TODO Auto-generated method stub
		return false;
	}
	//조합칸에서 검색키워드 초기화하는 역활하는 메소드
	@Override
	public HashMap<String, Double> InitSearchKeyword(ArrayList<String> inputlist) {
		HashMap<String, Double> searchkeyword =new HashMap<String, Double>();
		for(int i=0; i<inputlist.size() ;i++){
			searchkeyword.put(inputlist.get(i), 1.0);
		}
		return searchkeyword;
	}
	@Override
	public ArrayList<AccuracyDTO> changeAccuracy(HashMap<String, Double> searchKeyword) {
		AccuracyDTO accuracyDto;
		ArrayList<AccuracyDTO> returnList = new ArrayList<AccuracyDTO>();
		String url="";
		String result="";//해당 키워드로 검색한 스크래치 소스
		String keys[] = searchKeyword.keySet().toArray(new String[0]);
		for(int i=0; i<searchKeyword.size(); i++){
			String key = keys[i];
			System.out.println("changeAccuracy메소드 keyword : "+keys[i]);
			System.out.println("changeAccuracy메소드 크기 : "+searchKeyword.get(key));
			result=crawler.SearchManage(ConverterToString(keys[i]));
			
			
			//AccracyDTO에서 제외시키고 정확도 0으로 만듬
			if(result.contains("<h2><span> ‘"+ConverterToString(keys[i])+"’ </span> 에 일치하는 검색결과가 없습니다.</h2>")){//일치하지 않은경우
				System.out.println("check result contains");
			}else{//일치한경우
				//url 가져오는 부분
				Pattern pattern = Pattern.compile("href=\"(.*?)\"");
				Matcher matcher = pattern.matcher(result);
				
				while(matcher.find()){
					String urlResult=matcher.group(0).substring(6, matcher.group().length()-1);
					if((!urlResult.startsWith("#")) && urlResult.startsWith("http://") && !(urlResult.contains("zum"))){
						accuracyDto = new AccuracyDTO();
						accuracyDto.setAccuracy(accuracyDto.getAccuracy()*2);
						accuracyDto.setKeyword(ConverterToString(keys[i]));
						accuracyDto.setUrl(urlResult);
						returnList.add(accuracyDto);
					}
				}
			}
			
		}
				
		return returnList;
	}
	@Override
	public KeywordDTO converterToCombinaion(List<MemberKeywordCategoryDTO> inputList){
		KeywordDTO keywordDTO= new KeywordDTO();
		ArrayList<HashMap<String, Double>> returnList = new ArrayList<HashMap<String,Double>>();

		for(int i=0; i<inputList.size(); i++){
			HashMap<String, Double> inputData= new HashMap<String, Double>();
			if(inputList.get(i).getType().equals("id") || inputList.get(i).getType().equals("name") || inputList.get(i).getType().equals("email")    ){
				inputData.put(inputList.get(i).getContent(), 1.0);
			}
			else{
				inputData.put(inputList.get(i).getContent(), 0.0);
			}
			if(inputList.get(i).getType().equals("id")){
				keywordDTO.setId(inputData);
				returnList.add(keywordDTO.getId());
			}
			if(inputList.get(i).getType().equals("name")){
				keywordDTO.setName(inputData);
				returnList.add(keywordDTO.getName());
			}
			if(inputList.get(i).getType().equals("address")){
				keywordDTO.setAddress(inputData);
				returnList.add(keywordDTO.getAddress());
			}
			if(inputList.get(i).getType().equals("univschool")){
				keywordDTO.setUnivschool(inputData);
				returnList.add(keywordDTO.getUnivschool());
			}
			if(inputList.get(i).getType().equals("phone")){
				keywordDTO.setPhone(inputData);
				returnList.add(keywordDTO.getPhone());
			}
			if(inputList.get(i).getType().equals("birth")){
				keywordDTO.setBirth(inputData);
				returnList.add(keywordDTO.getBirth());
			}
			if(inputList.get(i).getType().equals("email")){
				keywordDTO.setEmail(inputData);
				returnList.add(keywordDTO.getEmail());
			}
		}
		keywordDTO.setList(returnList);
		return keywordDTO;
	}
	@Override
	public ArrayList<String> converterToMutation(String keyword) {
		ArrayList<String> returnList = new ArrayList<String>();
		int start=0;//숫자시작하는 자리수
		int end=0;//숫자끝나는 자리수
		int size=0;//숫자가 몇개포함되는지 크기
		boolean charOrIntFlag =false;//숫자사이에 문자가 포함이 되있는지 확인하는 boolean
		String returnTemp="";
		String[] returnTempArr={"",""};
		//start,end,size 찾기
		for(int i=0; i<keyword.length(); i++){
			int check=keyword.charAt(i);
			if(check>=48 && check<=57){
				if(size==0){
					start=i;
				}else{
					end=i;
				}
				size++;
			}
			else{
				returnTemp=returnTemp+keyword.charAt(i);
			}
		}
		System.out.println("start : "+start);
		System.out.println("end : "+end);
		System.out.println("size : "+size);
		if(size == 0){//숫자가 없다면
			returnList.add(returnTemp);
		}
		else if(size == 1 ){//숫자가 한개라면
			if(keyword.charAt(start) == '9'){//1개인 숫자가 9라면
				for(int i=0; i<keyword.length(); i++){
					if(start == i){
						returnTempArr[0] = returnTempArr[0]+"0";
						returnTempArr[1] = returnTempArr[1]+"8";
					}else{
						returnTempArr[0] = returnTempArr[0]+keyword.charAt(i);
						returnTempArr[1] = returnTempArr[1]+keyword.charAt(i);
					}
				}
				
				returnList.add(returnTempArr[0]);
				returnList.add(returnTempArr[1]);
				
			}else if(keyword.charAt(start) == '0'){//1개인 숫자가 0이라면
				for(int i=0; i<keyword.length(); i++){
					if(start == i){
						returnTempArr[0] = returnTempArr[0]+"9";
						returnTempArr[1] = returnTempArr[1]+"1";
					}else{
						returnTempArr[0] = returnTempArr[0]+keyword.charAt(i);
						returnTempArr[1] = returnTempArr[1]+keyword.charAt(i);
					}
				}
				
				returnList.add(returnTempArr[0]);
				returnList.add(returnTempArr[1]);

			}else{//1개인 숫자가 1,2,3,4,5,6,7,8이라면
				for(int i=0; i<keyword.length(); i++){
					if(start == i){
						int temp=Character.valueOf(keyword.charAt(start))-48;
						returnTempArr[0] = returnTempArr[0]+String.valueOf(temp+1);
						returnTempArr[1] = returnTempArr[1]+String.valueOf(temp-1);
					}else{
						returnTempArr[0] = returnTempArr[0]+keyword.charAt(i);
						returnTempArr[1] = returnTempArr[1]+keyword.charAt(i);
					}
				}
				returnList.add(returnTempArr[0]);
				returnList.add(returnTempArr[1]);
				
			}
		}else if(end-start+1 ==size){//숫자가 2개이상이고 사이에 문자가 포함이 되지 않았다면
			int temp=Integer.parseInt(keyword.substring(start,end+1));
			System.out.println("temp : "+temp);
			returnTempArr[0]=keyword.substring(0,start)+String.valueOf(temp+1)+keyword.substring(end,keyword.length()-1);
			returnTempArr[1]=keyword.substring(0,start)+String.valueOf(temp-1)+keyword.substring(end,keyword.length()-1);
			returnList.add(returnTempArr[0]);
			returnList.add(returnTempArr[1]);
		}else if(end-start >size){//숫자가2개이상이고 사이에 문자가 포함이 되어있다면
			returnList.add(keyword);
		}else{
			returnList.add(keyword);
		}
		return returnList;
	}

	@Override
	public String ConverterToString(String input) {
		String result="";
		System.out.println("변경전 값 : "+input);
		result=input.replace("]", "").replace("[", "");
		System.out.println("변경후 값 : "+result);
		return result;
	}

	@Override
	public ArrayList<AccuracyDTO> judgeInferior(ArrayList<String> list) {
		AccuracyDTO accuracyDto;
		ArrayList<AccuracyDTO> returnList = new ArrayList<AccuracyDTO>();
		String url="";
		String result="";//해당 키워드로 검색한 스크래치 소스
		for(int i=0; i<list.size(); i++){
			result=crawler.SearchManage(list.get(i));

			//AccracyDTO에서 제외시키고 정확도 0으로 만듬
			if(result.contains("<h2><span> ‘"+list.get(i)+"’ </span> 에 일치하는 검색결과가 없습니다.</h2>")){//일치하지 않은경우
				returnList.remove(i);
			}else{//일치한경우
				//url 가져오는 부분
				Pattern pattern = Pattern.compile("href=\"(.*?)\"");
				Matcher matcher = pattern.matcher(result);
				while(matcher.find()){
					String urlResult=matcher.group(0).substring(6, matcher.group().length()-1);
					if((!urlResult.contains("#") && urlResult.startsWith("http://") && !(urlResult.contains("zum") && !urlResult.contains("href") && !urlResult.contains("dic.naver.com") && !urlResult.contains("egloos.com")))){
						accuracyDto = new AccuracyDTO();
						accuracyDto.setAccuracy(1.0);
						accuracyDto.setKeyword(list.get(i));
						accuracyDto.setUrl(urlResult);
						returnList.add(accuracyDto);
					}
				}
			}
		}
		return returnList;
	}
	@Override
	public ArrayList<AccuracyDTO> judgeInferior(ArrayList<String> list,ArrayList<AccuracyDTO> list2) {
		String url="";
		String result="";//해당 키워드로 검색한 스크래치 소스
		AccuracyDTO accuracyDto;
		for(int i=0; i<list.size(); i++){
			result=crawler.SearchManage(list.get(i));

			//AccracyDTO에서 제외시키고 정확도 0으로 만듬
			if(result.contains("<h2><span> ‘"+list.get(i)+"’ </span> 에 일치하는 검색결과가 없습니다.</h2>")){//일치하지 않은경우
			}else{//일치한경우
				//url 가져오는 부분
				Pattern pattern = Pattern.compile("href=\"(.*?)\"");
				Matcher matcher = pattern.matcher(result);
				while(matcher.find()){
					String urlResult=matcher.group(0).substring(6, matcher.group().length()-1);
					if((!urlResult.contains("#")) && urlResult.startsWith("http://") && !(urlResult.contains("zum") && !(urlResult.contains("href")))) {
						accuracyDto = new AccuracyDTO();
						accuracyDto.setAccuracy(list2.get(i).getAccuracy()*2);
						accuracyDto.setKeyword(list.get(i));
						accuracyDto.setUrl(urlResult);
						list2.add(accuracyDto);
					}
				}
			}
		}
		return list2;
	}

	@Override
	public boolean containsHangul(String str) {
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(ch);
			if (UnicodeBlock.HANGUL_SYLLABLES.equals(unicodeBlock)
				|| UnicodeBlock.HANGUL_COMPATIBILITY_JAMO.equals(unicodeBlock) 
				|| UnicodeBlock.HANGUL_JAMO.equals(unicodeBlock))
				return true;
		}
		return false;
	}

	@Override
	public ArrayList<String> Scraper(String url, String keyword,String keyword2) throws JauntException {
		ArrayList<String> returnList = new ArrayList<String>();
	    UserAgent userAgent = new UserAgent();      //create new userAgent (headless browser)
	    userAgent.settings.autoSaveAsHTML = true;
	    userAgent.visit(url);       //visit google
	    userAgent.doc.apply(keyword+":"+keyword2);         //apply form input (starting at first editable field)
	    userAgent.doc.submit();
	    
	    Elements links = userAgent.doc.findEvery("<h3 class=r>").findEvery("<a>");   //find search result links 
	    for(Element link : links){ 
	    	returnList.add(link.getAt("href").replaceAll("amp;", ""));
	    }
	    return returnList;
	}

	
}
