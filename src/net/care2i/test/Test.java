package net.care2i.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import net.care2i.tempdto.SearchKeyword;
/**
 * <pre>
 * keyword 관련 정확도 정하는 테스트
 * </pre>
 * @author user
 *
 */
public class Test {

	public static void main(String[] args) {
		HashMap<String, Double> map = new HashMap<String, Double>();
		map.put("keyword1", 1.0);
		map.put("keyword2", 2.0);
		map.put("keyword3", 3.0);

		SearchKeyword search = new SearchKeyword();
		search.setSearchkey(map);
		
		System.out.println(search.getSearchkey().keySet());
	}

}
