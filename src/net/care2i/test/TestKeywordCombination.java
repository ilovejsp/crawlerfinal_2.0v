package net.care2i.test;

import java.util.ArrayList;
import java.util.HashMap;

import net.care2i.tempdto.KeywordDTO;
import net.care2i.webcrawler.Match;
/**
 * <pre>
 * 신상정보를 토대로 조합하는 테스트
 * </pre>
 * @author ilovejsp
 *
 */
public class TestKeywordCombination {

	public static void main(String[] args) {
		KeywordDTO keywordDTO= new KeywordDTO();//dto 선언
		
		//테스트데이터 추가
		ArrayList<HashMap<String, Double>> list = new ArrayList<HashMap<String,Double>>();
		
		HashMap<String, Double> id= new HashMap<String, Double>();
		id.put("ilovejsp", 1.0);
		keywordDTO.setId(id);
		
		HashMap<String, Double> name= new HashMap<String, Double>();
		name.put("김민제", 0.8);
		keywordDTO.setName(name);
		
		HashMap<String, Double> school= new HashMap<String, Double>();
		school.put("한국산업기술대학교", 0.5);
		keywordDTO.setUnivschool(school);
		
		list.add(keywordDTO.getId());
		list.add(keywordDTO.getName());
		list.add(keywordDTO.getUnivschool());
		
		keywordDTO.setList(list);
		
		Match match = new Match();
		//match.getcombination(keywordDTO);
		

	}

}
