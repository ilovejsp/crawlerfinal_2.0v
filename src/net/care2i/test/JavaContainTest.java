package net.care2i.test;

import static org.junit.Assert.*;

import org.junit.Test;

public class JavaContainTest {

	@Test
	public void test() {
		String text="abcdefg";
		String find="cde";
		assertEquals(true, text.contains(find));
	}

}
