package net.care2i.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.care2i.dbdao.CrawlerDAO;
import net.care2i.dbdao.CrawlerDAO;
import net.care2i.dbdto.MemberDTO;
import net.care2i.dbdto.MemberKeywordCategoryDTO;
/**
 * <pre>
 * MemberDao관련 테스트
 * </pre>
 * @author ilovejsp
 *
 */
public class MemberDAOTest {

	public static void main(String[] args) throws SQLException {
		CrawlerDAO dao = new CrawlerDAO();
		List<MemberKeywordCategoryDTO> list = new ArrayList<MemberKeywordCategoryDTO>();
		MemberKeywordCategoryDTO dto = new MemberKeywordCategoryDTO();
		list=dao.selectInformation(1);
		
		for(int i=0; i<list.size(); i++){
			System.out.println(list.get(i).getNo());
			System.out.println(list.get(i).getType());
			System.out.println(list.get(i).getContent());
			System.out.println("****************************");
			
		}

	}

}
