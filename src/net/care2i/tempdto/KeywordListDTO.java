package net.care2i.tempdto;

import java.util.ArrayList;

public class KeywordListDTO {
	private ArrayList<String> list = new ArrayList<String>();

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}
	

}
