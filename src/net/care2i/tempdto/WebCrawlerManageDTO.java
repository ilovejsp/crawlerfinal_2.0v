package net.care2i.tempdto;

/**
 * <pre>
 * 웹크롤러 관리자 클래스
 * </pre>
 * @author ilovejsp
 */
public class WebCrawlerManageDTO {
	private String status;//상태
	private int thread;//스레드개수
	private double starttime;//시작시간
	private double endtime;//죵료시간
	private boolean updatecheck;//업데이트시간
	private double implementtime;//실행시간
	private String[] urls;//해당 주소
	/**
	 * 
	 * @return 해당주소
	 */
	public String[] getUrls() {
		return urls;
	}
	/**
	 * 
	 * @param urls 해당주소
	 */
	public void setUrls(String[] urls) {
		this.urls = urls;
	}
	/**
	 * 
	 * @return 실행시간
	 */
	public double getImplementtime() {
		return implementtime;
	}
	/**
	 * 
	 * @param implementtime 실행시간
	 */
	public void setImplementtime(double implementtime) {
		this.implementtime = implementtime;
	}
	/**
	 * 
	 * @return 현재상태
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 
	 * @param status 현재상태
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 
	 * @return 스레드개수
	 */
	public int getThread() {
		return thread;
	}
	/**
	 * 
	 * @param thread 스레드개수
	 */
	public void setThread(int thread) {
		this.thread = thread;
	}
	/**
	 * 
	 * @return 시작시간
	 */
	public double getStarttime() {
		return starttime;
	}
	/**
	 * 
	 * @param starttime 시작시간
	 */
	public void setStarttime(double starttime) {
		this.starttime = starttime;
	}
	/**
	 * @return 종료시간
	 */
	public double getEndtime() {
		return endtime;
	}
	/**
	 * 
	 * @param endtime 종료시간
	 */
	public void setEndtime(double endtime) {
		this.endtime = endtime;
	}
	/**
	 * 
	 * @return 업데이트체크
	 */
	public boolean isUpdatecheck() {
		return updatecheck;
	}
	/**
	 * 
	 * @param updatecheck 업데이트체크
	 */
	public void setUpdatecheck(boolean updatecheck) {
		this.updatecheck = updatecheck;
	}
	
}
