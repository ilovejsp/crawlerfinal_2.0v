package net.care2i.tempdto;
/**
 * <pre>
 * 웹크롤러 1개의 스레드 DTO
 * </pre>
 * @author ilovejsp
 */
public class InformCollectorDTO {
	private String status;//상태
	private double starttime;//시작시간
	private double endtime;//죵료시간
	private String implementtime;//실행시간
	private String starturl;//시작URL
	private String returnurl;//리턴URL
	/**
	 * 
	 * @return 스레드 상태
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 
	 * @param status 스레드상태
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 
	 * @return 시작시간
	 */
	public double getStarttime() {
		return starttime;
	}
	/**
	 * 
	 * @param starttime 시작시간
	 */
	public void setStarttime(double starttime) {
		this.starttime = starttime;
	}
	/**
	 * 
	 * @return 종료시간
	 */
	public double getEndtime() {
		return endtime;
	}
	/**
	 * 
	 * @param endtime 종료시간
	 */
	public void setEndtime(double endtime) {
		this.endtime = endtime;
	}
	/**
	 * 
	 * @return 실행시간
	 */
	public String getImplementtime() {
		return implementtime;
	}
	/**
	 * 
	 * @param implementtime 실행시간
	 */
	public void setImplementtime(String implementtime) {
		this.implementtime = implementtime;
	}
	/**
	 * 
	 * @return 시작url
	 */
	public String getStarturl() {
		return starturl;
	}
	/**
	 * 
	 * @param starturl 시작url
	 */ 
	public void setStarturl(String starturl) {
		this.starturl = starturl;
	}
	/**
	 * 
	 * @return 최종url
	 */
	public String getReturnurl() {
		return returnurl;
	}
	/**
	 * 
	 * @param returnurl 종료url
	 */
	public void setReturnurl(String returnurl) {
		this.returnurl = returnurl;
	}
	
	

}
