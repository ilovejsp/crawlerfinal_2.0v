package net.care2i.tempdto;

import java.util.HashMap;
/**
 * <pre>
 * 키워드 검색 dto
 * </pre>
 * @author ilovejsp
 * 
 */
public class SearchKeyword {
	private HashMap<String, Double> searchkey;
	/**
	 * 
	 * @return keyword값(키워드,정확도)
	 */
	public HashMap<String, Double> getSearchkey() {
		return searchkey;
	}
	/**
	 * 
	 * @param searchkey keyword값(키워드,정확도)
	 */
	public void setSearchkey(HashMap<String, Double> searchkey) {
		this.searchkey = searchkey;
	}
	
}
