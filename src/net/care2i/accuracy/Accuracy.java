package net.care2i.accuracy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.care2i.tempdto.AccuracyDTO;

/**
 * <pre>
 * 정확도에 따라 일치,유사,없음으로 나뉨
 * </pre>
 * @author ilovejsp
 */
public interface Accuracy {
	/**
	 * <pre>
	 * keyword를 가지고 검색을 해서 정확도를 변경해주는 역활
	 * </pre>
	 * @param HashMap<String, Double> searchKeyword 검색하는 키워드
	 * @return ArrayList<AccuracyDTO> list형태로 정확도를 리턴
	 */
	public ArrayList<AccuracyDTO> changeAccuracy(HashMap<String, Double> searchKeyword);
	public ArrayList<AccuracyDTO> judgeInferior(ArrayList<String> list);
	public ArrayList<AccuracyDTO> judgeInferior(ArrayList<String> list,ArrayList<AccuracyDTO> returnList);
	
}
