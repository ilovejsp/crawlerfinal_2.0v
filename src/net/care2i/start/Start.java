package net.care2i.start;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jaunt.JauntException;

import net.care2i.dbdao.CrawlerDAO;
import net.care2i.dbdto.MemberKeywordCategoryDTO;
import net.care2i.tempdto.AccuracyDTO;
import net.care2i.tempdto.KeywordDTO;
import net.care2i.test.BNDM;
import net.care2i.tree.Binary;
import net.care2i.tree.BinaryTree;
import net.care2i.webcrawler.Crawler;
import net.care2i.webcrawler.Match;

public class Start {
	private static String email = "";// 크롤링할 email
	private static List<MemberKeywordCategoryDTO> list;// 멤버키워드카테고리
	private static CrawlerDAO dao;// db연동 dao
	private static Crawler crawler;// crawler
	private static Match match;// match
	private static KeywordDTO keywordDTO;// 신상정보 데이터
	private static ArrayList<String> searchKeywordStoreList;// 검색 키워드 저장값
	private static HashMap<String, Double> searchKeyword;// 검색키워드
	private static ArrayList<AccuracyDTO> accuracyDto;// 트리로 저장할 객체
	private static BNDM bndm;// 문자열 검색 알고리즘
	private static Binary binary;// 최종저장할 이진트리, 정확도,링크,키워드로 구성됨
	private static BinaryTree binaryTree;// 이진트리 기능
	private static ArrayList<String> memberInformation;// 멤버정보
	private static int count = 0;// 리소스 누출 전체 카운트
	private static String mainKeyword="";
	private static String[] gDelicater={"kr","net","com"};
	private static ArrayList<String> returnList;
	private static boolean returnChecker=false;//최종 리턴 체크 값
	private static int memberNo=0;

	public Start(String email) {
		this.email = email;// 이메일초기화
		list = new ArrayList<MemberKeywordCategoryDTO>();// 이메일로부터 갖고올 신상정보
		dao = new CrawlerDAO();// database access 부분
		crawler = new Crawler();// 크롤러 초기화
		match = new Match();// 문자열관련 클래스 초기화
		keywordDTO = new KeywordDTO();//
		searchKeywordStoreList = new ArrayList<String>();
		searchKeyword = new HashMap<String, Double>();
		accuracyDto = new ArrayList<AccuracyDTO>();// 트리로 저장할 객체 초기화
		bndm = new BNDM();// 믄자열 검색 초기화
		binary = new Binary();// 이진트리 초기화
		binaryTree = new BinaryTree();// 이진트리 기능초기화
		memberInformation = new ArrayList<String>();
		memberInformation = dao.getInformationToList(email);// 해당 email 멤버정보를
		mainKeyword=dao.getInformationMainKeyword(email);
		returnList=new ArrayList<String>();
		memberNo=dao.selectMemberNo(email);
	}

	// 조합
	public void combination() {
		searchKeywordStoreList = match.getcombination(memberInformation);// 맨처음
	}
	// 열등한 개체 제거
	public void inferior() {
		accuracyDto = match.judgeInferior(searchKeywordStoreList);
		// 이진트리에저장
		for (int i = 0; i < accuracyDto.size(); i++) {
			binary.setAccuracy(accuracyDto.get(i).getAccuracy());
			binary.setKeyword(accuracyDto.get(i).getKeyword());
			binary.setLink(accuracyDto.get(i).getUrl());
			binaryTree.insert(binary);
		}
	}
	// 우수한 개체판별
	public void superior() throws UnknownHostException {
		for (int i = 0; i < accuracyDto.size(); i++) {
			if (!accuracyDto.get(i).getUrl().contains("href")&& match.containsHangul(accuracyDto.get(i).getUrl()) == false) {
				System.out.println("check geturl : "+ accuracyDto.get(i).getUrl());
				String scratch = crawler.ScratchManage(accuracyDto.get(i).getUrl());
				for (int listNum = 0; listNum < memberInformation.size(); listNum++) {
					// 신상정보와 유사한가? 맞다면 정확도를 2배해주고 트리에 저장한다.
					if (bndm.findAll(memberInformation.get(listNum), scratch) == true) {
						binary.setAccuracy(accuracyDto.get(i).getAccuracy() * 2);
						binary.setKeyword(accuracyDto.get(i).getKeyword());
						binary.setLink(accuracyDto.get(i).getUrl());
						binaryTree.insert(binary);
					} else {
						System.out.println("no match !");
					}
				}
			}

		}

	}

	// 돌연변이
	/*
	public void mutation() {
		List<String> tempList = new ArrayList<String>();
		ArrayList<String> addTempList = new ArrayList<String>();
		tempList = memberInformation;

		for (int i = 0; i < tempList.size(); i++) {
			addTempList = match.converterToMutation(tempList.get(i));
			accuracyDto = match.judgeInferior(addTempList);
		}
	}
	*/
	public void gScraper() throws JauntException{
		ArrayList<String> tempList = new ArrayList<String>();
		for(int i=0; i<gDelicater.length; i++){
			tempList=match.Scraper("http://www.google.co.kr", mainKeyword, gDelicater[i]);
			for(int j=0; j<tempList.size(); j++){
				Binary binary =new Binary();
				binary.setAccuracy(10.0);
				binary.setKeyword(mainKeyword+":"+gDelicater[i]);
				binary.setLink(tempList.get(j));
				binaryTree.insert(binary);
			}
		}
	}
	public boolean insertLink(){
		returnList=binaryTree.printLink();
		for(int i=0; i<returnList.size(); i++){
			dao.insertLink(returnList.get(i),memberNo);
		}
		return true;
	}


	// 메인메소드
	public boolean start() throws UnknownHostException, JauntException {
		combination();
		inferior();
		superior();
		//mutation();
		gScraper();
		returnChecker=insertLink();
		return returnChecker;
	}

}
