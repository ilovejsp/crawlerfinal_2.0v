package net.care2i.dbdao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 







import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 



import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import net.care2i.dbdto.MemberDTO;
import net.care2i.dbdto.MemberKeywordCategoryDTO;
import net.care2i.tempdto.KeywordListDTO;
 /**
  * <pre>
  * 실제로 데이터베이스 쿼리문을 가지고 있는 클래스
  * </pre>
  * @author ilovejsp
  *
  */
public class CrawlerDAO {
    private static DataSource ds = CrawlerDataSourceFactory.getMySQLDataSource();
	private static Connection conn;
    
	
	/**
	 * <pre>
	 * 전체 멤버의 정보를 가지고 오는 메소드
	 * </pre>
	 */
    public void selectMember() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select member_no,member_email,member_passwd from oir_member");
            while(rs.next()){
                System.out.println("no="+rs.getInt("member_no")+", email="+rs.getString("member_email")+", passwd="+rs.getString("member_passwd"));
            }
            CrawlerDAOClose.close(rs);
            CrawlerDAOClose.close(stmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * <pre>
     * 조인을 써서 회원들의 신상정보를 가지고 오는 메소드
     * </pre>
     * @param no 회원번호 key
     * @return 회원정보객체
     */
    public List<MemberKeywordCategoryDTO> selectInformation(int no){
    	List<MemberKeywordCategoryDTO> list= new ArrayList<MemberKeywordCategoryDTO>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MemberKeywordCategoryDTO dto;
        try {
        	String query="SELECT M.MEMBER_NO AS NO,KC.KEYWORD_CATEGORY_TYPE AS TYPE,KC.KEYWORD_CATEGORY_CONTENT AS CONTENT "
            		+ "FROM OIR_MEMBER AS M, OIR_MEMBER_KEYWORD AS MK, OIR_KEYWORD_CATEGORY AS KC "
            		+ "WHERE M.MEMBER_NO=MK.MEMBER_NO AND MK.KEYWORD_CATEGORY_NO=KC.KEYWORD_CATEGORY_NO "
            		+ "AND M.MEMBER_NO=?";
            conn = ds.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1,no);
            rs = pstmt.executeQuery();
            while(rs.next()){
            	dto=new MemberKeywordCategoryDTO();
            	dto.setNo(rs.getInt("NO"));
            	dto.setType(rs.getString("TYPE"));
            	dto.setContent(rs.getString("CONTENT"));
            	list.add(dto);
            }
            CrawlerDAOClose.close(rs);
            CrawlerDAOClose.close(pstmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	return list;
    }
    public List<MemberKeywordCategoryDTO> getInformation(String email){
    	List<MemberKeywordCategoryDTO> list= new ArrayList<MemberKeywordCategoryDTO>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MemberKeywordCategoryDTO dto;
        try {
        	String query="SELECT KC.KEYWORD_CATEGORY_NO AS NO,KC.KEYWORD_CATEGORY_TYPE AS TYPE,KC.KEYWORD_CATEGORY_CONTENT AS CONTENT "+
        					"FROM OIR_MEMBER AS M, OIR_MEMBER_KEYWORD AS MK, OIR_KEYWORD_CATEGORY AS KC"+
        						 "M.MEMBER_NO=MK.MEMBER_NO AND MK.KEYWORD_CATEGORY_NO=KC.KEYWORD_CATEGORY_NO"+
        						 	"AND M.MEMBER_EMAIL=?";

            conn = ds.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1,email);
            rs = pstmt.executeQuery();
            while(rs.next()){
            	dto=new MemberKeywordCategoryDTO();
            	dto.setNo(rs.getInt("NO"));
            	dto.setType(rs.getString("TYPE"));
            	dto.setContent(rs.getString("CONTENT"));
            	list.add(dto);
            }
            CrawlerDAOClose.close(rs);
            CrawlerDAOClose.close(pstmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	return list;
    }
    
	public List<MemberKeywordCategoryDTO> getInformation(int no){
		CrawlerDAO dao = new CrawlerDAO();
		List<MemberKeywordCategoryDTO> list = new ArrayList<MemberKeywordCategoryDTO>();
		MemberKeywordCategoryDTO dto = new MemberKeywordCategoryDTO();
		list=dao.selectInformation(no);
		return list;
	}
	/*
	public List<MemberKeywordCategoryDTO> getInformation(String email){
		CrawlerDAO dao = new CrawlerDAO();
		List<MemberKeywordCategoryDTO> list = new ArrayList<MemberKeywordCategoryDTO>();
		MemberKeywordCategoryDTO dto = new MemberKeywordCategoryDTO();
		list=dao.selectInformation(email);
		return list;
	}
	*/
    public ArrayList<String> getInformationToList(String email){
    	ArrayList<String> list= new ArrayList<String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
        	String query="SELECT KC.KEYWORD_CATEGORY_CONTENT AS CONTENT "+
        					"FROM OIR_MEMBER AS M, OIR_MEMBER_KEYWORD AS MK, OIR_KEYWORD_CATEGORY AS KC "+
        						 "WHERE M.MEMBER_NO=MK.MEMBER_NO AND MK.KEYWORD_CATEGORY_NO=KC.KEYWORD_CATEGORY_NO AND M.MEMBER_EMAIL=?";
            conn = ds.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1,email);
            rs = pstmt.executeQuery();
            while(rs.next()){
            	list.add(rs.getString("CONTENT"));
            }
            CrawlerDAOClose.close(rs);
            CrawlerDAOClose.close(pstmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	return list;
    }
    public String getInformationMainKeyword(String email){
    	String result="";
    	int firstCheck=0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
        	String query="SELECT KC.KEYWORD_CATEGORY_CONTENT AS CONTENT "+
        					"FROM OIR_MEMBER AS M, OIR_MEMBER_KEYWORD AS MK, OIR_KEYWORD_CATEGORY AS KC "+
        						 "WHERE M.MEMBER_NO=MK.MEMBER_NO AND MK.KEYWORD_CATEGORY_NO=KC.KEYWORD_CATEGORY_NO AND M.MEMBER_EMAIL=?";
            conn = ds.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1,email);
            rs = pstmt.executeQuery();
            while(rs.next()){
            	if(firstCheck==0){
                	result=rs.getString("CONTENT");
            		firstCheck=1;
            	}
            }
            CrawlerDAOClose.close(rs);
            CrawlerDAOClose.close(pstmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	return result;
    }
    
    public boolean insertLink(String link,int memberNo){
        PreparedStatement pstmt = null;
        int returnInt=0;
        boolean returnValue=false;
        try {
        	String query="INSERT INTO OIR_LINK(MEMBER_NO,LINK_LINK,LINK_ACCURACY,LINK_PRIORITY) VALUES(?,?,1,1) ";
            conn = ds.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1,memberNo);
            pstmt.setString(2,link);
            returnInt=pstmt.executeUpdate();
            if(returnInt!=0){
            	returnValue=true;
            }
            CrawlerDAOClose.close(pstmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return returnValue;
    }
     
    
    public int selectMemberNo(String email){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int result=0;
        try {
        	String query="SELECT MEMBER_NO FROM OIR_MEMBER WHERE MEMBER_EMAIL=?";
            conn = ds.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1,email);
            rs = pstmt.executeQuery();
            while(rs.next()){
            	result=rs.getInt(1);
            }
            CrawlerDAOClose.close(rs);
            CrawlerDAOClose.close(pstmt);
            CrawlerDAOClose.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	return result;
    }


 
}