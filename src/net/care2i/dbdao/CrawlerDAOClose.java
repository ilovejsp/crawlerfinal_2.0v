package net.care2i.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * <pre>
 * Data Access Object�� �����ִ°͵��� close�ϴ� Ŭ����
 * </pre>
 * @author ilovejsp
 *
 */
public class CrawlerDAOClose {
	/**
	 * <pre>
	 * connection��ü�� �ݴ� �޼ҵ�
	 * </pre>
	 * @param conn connection��ü
	 */
    public static void close(Connection conn){
    	try{
    		if(conn!= null){
    			conn.close();
    			conn=null;
    		}
    	}catch(SQLException se){
    		System.out.println("conn error");
    	}
    }
    /**
     * <pre>
     * statement��ü�� �ݴ� �޼ҵ�
     * </pre>
     * @param stmt Statement ��ü
     */
    public static void close(Statement stmt){
    	try{
    		if(stmt!= null){
    			stmt.close();
    			stmt=null;
    		}
    	}catch(SQLException se){
    		System.out.println("stmt error");
    	}
    	
    }
    /**
     * <pre>
     * PreparedStatement��ü�� �ݴ� �޼ҵ�
     * </pre>
     * @param pstmt PreparedStatement��ü
     */
    public static void close(PreparedStatement pstmt){
    	try{
    		if(pstmt!= null){
    			pstmt.close();
    			pstmt=null;
    		}
    	}catch(SQLException se){
    		System.out.println("pstmt error");
    	}
    	
    }
   
    /**
     * <pre>
     * ResultSet��ü�� �ݴ� �޼ҵ�
     * </pre>
     * @param rs resultset��ü
     */
    public static void close(ResultSet rs){
    	try{
    		if(rs!= null){
    			rs.close();
    			rs=null;
    		}
    	}catch(SQLException se){
    		System.out.println("rs error");
    	}
    	   	
    }
 

}
