package net.care2i.rearrage;

import java.util.List;

/**
 * <pre>
 * 우선순위에 따라 재배열하는 인터페이스
 * </pre>
 * @author ilovejsp
 */
public interface Rearrange {
	/**
	 * <pre>
	 * 우선순위에 따라 재배열하는 메소드
	 * </pre>
	 * @param list 키워드값들
	 * @param priority 우선순위값
	 * @return 우선순위로 재배열된 list값들
	 */
	public List<String> RearrangeResult(List<String> list,int priority);
}
